
source("simfunctions.R")

# {{{1 Try single runs

simulate_anderson(task="tvtask", alpha=2.333, nlab=-1, bias=0, a0=10, alab=1, lambda0=1, sigma0=0.15, tau=0.05, plotting=T, order="interspersed", echo=F)

# }}}1
#
# {{{1 Just simulate exp 1
runs <- expand.grid(task="tvtask",
                    nlab=c(0,4,16,-1), 
                    order=c("interspersed"),
                    sigma0=c(.25),
                    a0=c(16),
                    alab=c(.5),
                    lambda0=c(1),
                    alpha=c(.7/.3),
                    tau=c(.05), 
                    bias_sd=c(0),
                    encoding=c("encodeactual"))
runs <- subset(runs, ! ((alpha==1 & order!="interspersed") | (alpha==1 & nlab==4)))
nrow(runs)

nreps <- 300
ofile <- "/dev/null"
sims <- run_sims(runs, nreps, ofile)
sims$nlab[sims$nlab==-1] <- Inf

counts <- ddply(sims, .(nlab, alpha, sigma0, a0, alab, lambda0, tau, order, bias_sd, encoding), function(x) summary(x$BestFit))
counts$twod <- counts$"2D"
counts
# }}}1

# {{{1 Run sims across conditions.
runs <- expand.grid(task="tvtask",
                    nlab=c(0,4,16,-1), 
                    order=c("interspersed", "labeledfirst", "labeledlast"),
                    sigma0=c(.125),
                    a0=c(30),
                    alab=c(1),
                    lambda0=c(1),
                    alpha=c(1, .7/.3),
                    tau=c(.05), 
                    bias_sd=c(0, 1.5),
                    encoding=c("encodeactual"))
runs <- subset(runs, ! (((nlab==4 | alpha==1) & order!="interspersed") | (alpha==1 & nlab==4) | (order=="labeledfirst" & nlab!=16)))
nrow(runs)

nreps <- 101
ofile <- "data/forpub"
sims <- read.csv(ofile)
#sims <- run_sims(runs, nreps, ofile)
sims$nlab[sims$nlab==-1] <- Inf

counts <- ddply(sims, .(nlab, alpha, sigma0, a0, alab, lambda0, tau, order, bias_sd, encoding), function(x) summary(x$BestFit))
counts$twod <- counts$"2D"
# }}}1

# {{{1 Fitting model fits to data
# {{{2 Experiment 1

get.expone.proportions <- function(df) {
      denom <- function(x) x$twod + x$Unimodal + x$Bimodal #+ x$Null
      get.condition.proportions <- function(cond.df) {
        denom <- denom(cond.df)
        data.frame(
          bimod = cond.df$Bimodal / denom,
          unimod = cond.df$Unimodal / denom,
          twod = cond.df$twod / denom)
      }
      
      ddply(df, .(nlab), get.condition.proportions)
}
params <- .(sigma0, a0, alab, lambda0, tau, order, bias_sd, encoding)

expone <- subset(counts, order=="interspersed" & alpha>1)
expone.condition.proportions <- ddply(expone, params, get.expone.proportions)

score.run <- function(df) {
  bimod.props <- c(0.5714,.442857, .3, .3)
  unimod.props <- c(0.257142,.22857, .3, .2)
  twod.props <- c(.1714286, .32857, .4, .5)
  ssqerr <- sum(c(df$bimod-bimod.props, df$unimod-unimod.props, df$twod-twod.props) ^ 2)
  data.frame(sqerr=ssqerr)
}
model.fits.to.data <- ddply(expone.condition.proportions, params, score.run)
model.fits.to.data[with(model.fits.to.data, order(-sqerr)),]

head(model.fits.to.data[with(model.fits.to.data, order(sqerr)),])
head(subset(model.fits.to.data[with(model.fits.to.data, order(sqerr)),], bias_sd==0))

rate.bimodality.effect <- function(df) {
  fourlab <- subset(df, nlab==4)
  sixteenlab <- subset(df, nlab==16)
  data.frame(bimodeffect=fourlab$bimod - sixteenlab$bimod)
}

bimod.effect <- ddply(expone.condition.proportions, params, rate.bimodality.effect)
subset(bimod.effect, bias_sd==1.5)
ddply(bimod.effect, .(sigma0), summarise, mean(bimodeffect))
ddply(bimod.effect, .(bias_sd), summarise, mean(bimodeffect))
# }}}2
# }}}1

# {{{1 Plots
# {{{2 Setup
counts.melted <- melt(counts,
                      id=c("nlab", "alpha", "tau", "order", "encoding", "bias_sd", "sigma0", "a0"),
                      measure.vars=c("2D", "Bimodal", "Unimodal", "Null"),
                      variable.name="bestfit",
                      value.name="count")

#counts.melted$groupingparam <- do.call(paste, c(counts.melted[c("bestfit", "encoding")], sep = ":"))
#counts.melted

highalpha <- subset(counts.melted, alpha>1)[1,'alpha']
lowalpha <- 1
# }}}2

# {{{2 Experiment 1
plotdata <- subset(counts.melted, alpha==highalpha & order=="interspersed")
ggplot(plotdata) + geom_line(aes(x=factor(nlab), y=count, group=bias_sd, linetype=factor(bias_sd)))  + facet_grid(~bestfit)
ggsave("exp1.pdf")
# }}}2

# {{{2 Experiment 2
identify.exp2.cond <- function(row) {
    if (row["order"]!="interspersed") return(NA)
    nlab <- as.numeric(row["nlab"]) 
    if (is.infinite(nlab)) nlab <- "All"
    alpha <- as.numeric(row["alpha"]) 
    if (alpha==1) {
        return(paste(nlab, "-lab-resp", sep=""))
    } else if (nlab==16) {
        return(paste(nlab, "-lab-noresp", sep=""))
    } else return(NA)
}

plot.data <- subset(counts.melted, order=="interspersed")
plot.data$cond <- factor(apply(plot.data, 1, identify.exp2.cond))
plot.data$cond <- factor(plot.data$cond, levels=levels(plot.data$cond)[c(1,3,2,4)])
plot.data <- subset(plot.data, ! is.na(cond) )

ggplot(plot.data) + geom_line(aes(x=cond, y=count, group=bias_sd, linetype=factor(bias_sd)))  + facet_grid(~bestfit)
ggsave("exp2.pdf")
# }}}2

# {{{2 Experiment 3
identify.exp3.cond <- function(row) {
    nlab <- as.numeric(row["nlab"]) 
    order <- row["order"]
    if (nlab==0 & order=="interspersed") return("US")
    if (order=="interspersed") return(NA)
    alpha <- as.numeric(row["alpha"]) 
    if (alpha==1) return(NA)
    if (is.infinite(nlab) & order=="labeledlast") return("FS-lablast")
    if (nlab == 16) {
      if (order=="labeledfirst") return("SS-labfirst")
      if (order=="labeledlast") return("SS-lablast")
    }
    return(NA)
}
plot.data <- subset(counts.melted, alpha==highalpha)
plot.data$cond <- factor(apply(plot.data, 1, identify.exp3.cond))
plot.data$cond <- factor(plot.data$cond, levels=levels(plot.data$cond)[c(4,3,2,1)])
plot.data <- subset(plot.data, ! is.na(cond))

ggplot(plot.data) + geom_line(aes(x=cond, y=count, group=bias_sd, linetype=factor(bias_sd)))  + facet_grid(~bestfit)
ggsave("exp3.pdf")
# }}}2
# }}}1
